#GIT GUIDE 


#### title
Command description

```
git command x
```

#### clone repo
```
git clone git@bitbucket.org:repo/repo.git
```

#### pull upstream

```
git pull upstream branch
```
#### pull rebase commits
```
git pull --rebase upstream branch
```
#### undo last pull
```
git reset --hard ORIG_HEAD
```

#### revert commit
```
git revert <commit-hash>
```

#### delete branch
```
git branch -d branchname
```