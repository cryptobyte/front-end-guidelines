## General Front-End Guidelines
All the guidelines are open to contribution.

## System requirements
* [MacDown ![](https://dl.dropboxusercontent.com/u/257359/static/img/download.png)][1]

* [MacDown Alternatives for Windows ![](https://dl.dropboxusercontent.com/u/257359/static/img/download.png)][2]

#### Syntax Reference
* [Markdown ![](https://dl.dropboxusercontent.com/u/257359/static/img/download.png)][3]

#### Mac OS X
  * [Homebrew ![](https://dl.dropboxusercontent.com/u/257359/static/img/download.png)][4]
  * Git (`brew install git`)

#### Windows
  * [Git for Windows ![](https://dl.dropboxusercontent.com/u/257359/static/img/download.png)][5]



## Step-by-step Installation
This section is very basic and simple, but just in case:

Find a suitable location for the project.

    $ cd /Users/<USERNAME>/televisa

Fork this repository into your own `namespace` by visiting the URL below:

> [https://bitbucket.org/televisa/front-end-guidelines/fork][6]

Clone your recently forked repository using the SSH URI. Update the command below with the URL of your fork, which can be found in the top right corner of your BitBucket namespace:

    $ git clone git@bitbucket.org:<USERNAME>/front-end-guidelines.git

Switch to the project's directory:

    $ cd front-end-guidelines

Add `cryptobyte/front-end-guidelines` as the upstream repository. This will allow you to keep your namespace up to date with upstream updates.

    $ git remote add upstream git@bitbucket.org:televisa/front-end-guidelines.git
  
##### MacDown Configuration

Open MacDown configuration by pressing `CMD` + `,` and make sure you have the following options selected in the *Markdown* tab:

![MacDown Configuration Screenshot](https://photos-2.dropbox.com/t/2/AADzT3guopA-pDQyET7ioQ63kgjg_A6_qINPyQW0QJki0A/12/3507886/png/1024x768/3/1426611600/0/2/mcdwn_config.png/CK6N1gEgASACIAMoASgC/u0SmGHuM7xEKodTwUoUolVXCqCMyquEJwZpvFXoIoXw)

And that's it, You are done!


  
## JavaScript [Completed]
## AngularJS
## CSS/less
## HTML


[1]: http://macdown.uranusjr.com/
[2]: http://alternativeto.net/software/macdown/?platform=windows
[3]: http://daringfireball.net/projects/markdown/syntax
[4]: http://brew.sh/#install
[5]: http://msysgit.github.io/
[6]: https://bitbucket.org/cryptobyte/front-end-guidelines/fork