# Veo JavaScript Front-End Guidelines 
NEVER FORGET THE SEMICOLONS; 
## JavaScript

#### Code Style

* [JS-001 Whitespace](#JS-001)
* [JS-100 Variables](#JS-100)
* [JS-101 Type String](#JS-101)
* [JS-102 Type Array](#JS-102) 
* [JS-103 Type Object](#JS-103)
* [JS-104 Functions](#JS-104)
* [JS-105 Conditionals](#JS-105)
* [JS-106 Blocks](#JS-106)
* [JS-107 Comments](#JS-107)
* [JS-108 Type Casting](#JS-108)
* [JS-109 Naming Conventions](#JS-109)
* [JS-110 Accessors](#JS-110)
* [JS-111 Modules](#JS-111)
* [JS-112 Eval is Evil](#JS-112)
* [JS-113 Type Checking](#JS-113)


#### <a name="js-basic"></a> JavaScript

###### <a name="JS-001"></a> [JS-001] Whitespace
* For identantion tab width should be two spaces, configure your editor to use spaces.

~~~js
// Two spaces indentation
function foo() {
  return true;
}
~~~
* Operators should be separated by one space

~~~js
var a = 1 + b;

// Antipattern
var a=1+b;
~~~

* Always leave a blank line after and before statements

~~~js
var type = 'post';

if (type === 'get') {
  // Stuff
}

return true;
~~~

* Always leave one space before the leading brace 

~~~js
function functionName() {
}

app.stuff(function() {
  // code
});
~~~

###### <a name="JS-100"></a> [JS-100] Variables
* Use var when you declare variables. All undeclared variables become part of the global namespace so avoid polluting the global variable .
 
~~~js 
var specialVariable = 1010;

// Antipattern 
specialVariable = 1010;
~~~

* Declare each variable using the var keyword.

~~~js 
var pages = 80;
var blocks = 1080;
var busy = true;
var width = 680;
var reference;

// Avoid 
var 
  pages = 80,
  blocks = 1080,
  busy = true,
  width = 680,
  reference;
~~~

* Assign variables at the top of their scope 

~~~js
function FooController() {
  var items = [];
  var length = 21;
  var value;
  
  if (length < 40) {
    value = 20;
  }
  
  return value;
}

// Avoid 
function FooController() {
  var items = [];
  var length = 21;
  
  if (length < 40) {
   var value = 20;
  }
  
  return value;
}
~~~

###### <a name="JS-101"></a> [JS-101] Strings
* Use single quotes for strings

~~~js 
var name = 'Jose Luis';
~~~
* In case you have to create a long string please do it this way 

~~~js
var error = 'This is a super long error that was thrown because ' +
  'of Batman. When you stop to think about how Batman ' +
  'anything to do with this, you would get nowhere';
~~~

or 

~~~js 
var error = [
  'This is a super long error that was thrown because ',
  'of Batman. When you stop to think about how Batman ',
  'anything to do with this, you would get nowhere'
].join('');
~~~


###### <a name="JS-102"></a> [JS-102] Arrays
* Use literal syntax

~~~js 
var collection = [];

// Antipattern
var collection = new Array();
~~~

###### <a name="JS-103"></a> [JS-103] Objects
* Use literal xyntax

~~~js 
var person = {};
	
//Antipattern
var person = new Object();
~~~
* Use normal loops for traversing an array,  supported built-in functions loops or framework function loops.

~~~js
function printArray(arr) {
  for (var i = 0, length = arr.length; i < length; i++) {
    print(arr[i]);
  }
}
// Avoid for-in loop
function printArray(arr) {
  for (var key in arr) {
    print(arr[key]);
  }
}
~~~

* Use dot notation to access or assign values

~~~js 
var person = { name: 'Luis' };
person.name; // => Luis
person.name = 'Jose';
	
// antipattern
person['name'] = 'Carlos';
~~~

* If we have a lot of properties for an Object group them in the same literal

~~~js 
var user = {
  id: 'aa7s67df67sdf6',
  name: 'Dora',
  lastname: 'Exploradora',
  phone: '3052371130',
};

// Avoid
var user = {};
user.id = 'aa7s67df67sdf6';
user.name = 'Dora';
user.lastname = 'Exploradora';
user.phone = '3052371130';
~~~

* Use subscript [] notation to access properties with a variable

~~~js 
var user = {
  id: 'aa7s67df67sdf6',
  name: 'Dora'
};

function getProperty(key) {
  return user[key];
}

getProperty('id'); // => 'aa7s67df67sdf6'
~~~

* Test if properties are existent

~~~js
var data = { name: 'Merlin' };
if ('name' in object) {
  // do stuff
}

// Avoid
if (object.name !== undefined) {
}
~~~

###### <a name="JS-104"></a> [JS-104] Functions

* For Immediately-Invoked function expression (IIFE)

~~~js
(function () {
  // Do stuff
}());
~~~
For more details check the AngularJS.

###### <a name="JS-105"></a> [JS-105] Conditionals 

* Use === and !== 

~~~js 
if (a === b) {
  // Do stuff
}

if (x !== y) {
  // Do stuff
}

// Antipattern 
if (a == b) {
}
if (x != y) {
}

~~~

###### <a name="JS-106"></a> [JS-106] Blocks 
~~~js 
// Good way
if (isBusy) return true;

// Good way
if (isBusy) {
  return true;
}

// Avoid
if (isBusy)
  return true;

// Avoid putting the curly braces in a new line
if (isBusy) 
{
  // stuff 
}
~~~
* Multiple blocks if, else 

~~~js 
// Good 
if (isBusy) {
  // stuff
} else {
  return false;
}

// Avoid 
if (isBusy) {
  // stuff
} 
else {
  // stuff
}
~~~
* Ternary operator conditional evaluates to an expression , use it only for value assignation.

~~~js
// Good
var pageOrientation =  ( orientation > 0 ) ? 'portrait' : 'landscape';

// Avoid 
ifThis() ? doThis() : thenThis();

// Don't chain 
var value = a > b ? ( x == y ? true : false ) : ( c > 0 ? false : true) ;
~~~


###### <a name="JS-107"></a> [JS-107] Comments 
* Multiple-line comment should be used for module descriptions.See more documentation params [jsDoc](http://usejsdoc.org/)

~~~js

/**
 * @namespace AppController
 * @desc module description
*/
function AppController() {
  var name = '';
  
  /**
  * @method set
  * @descripton 
  * @param {String} value description
  * @returns {Boolean}
  */
  function setValue(value) {
    name = value;
    return true;
  }
}
~~~ 

* Single line comments. Keep the line comment above the subject of the comment and leave an empty line before. 

~~~js
function getColumns() {
  var columns;
  
  // number of columns
  columns = collection / 40;
  
  return columns;
}
~~~

* Prefix your comments with **TODO**, **FIXME** to annotate problems and solutions. 

~~~js
/**
 * @param {String}
 * @returns {String}
*/
// FIXME: please fix this it's returning an array
function reverseString(input) {
  // FIXME: please use single quotes for strings
  return input.split("").reverse();
}

// TODO: return an array of assets from the API
function getAssets() {
  var url = '/api/assets/';
}
~~~

* If you are using chaining pattern use two space indentation.

~~~js
$('#ul')
  .find('li')
  .addClass('active');
~~~

###### <a name="JS-108"></a> [JS-108] Type Casting
* Recommended way to cast data

~~~js
// To String
var number = 13;
'' + number; // => '13'

// To Number
var string = '12';
parseInt(string, 10); // => 12
Number(string); // => 12
// The difference
parseInt('12a'); // => 12
Number('12a'); // => NaN

// To Boolean
var zero = 0;
Boolean(zero); // => false
!!zero; // => false
~~~

###### <a name="JS-109"></a> [JS-109] Name conventions 
* **Camel Case** for variables, functions, methods and instances

~~~js
// Variable
var numberColumns = 12;

// Property
this.rightProperty = right;

// Function Name
function superFunction() {
}

// Method Name
this.methodName = function() {
};

~~~

* **Pascal Case** use it to name constructors or classes.

~~~js
function Asset() {
  var _self = this;
  _self.name = 'Nemo';
  _self.getName = function() {
    return _self.name;
  };
}
~~~

* **Private Members** for private properties use the underscore `_` 

~~~js
function UserController() {
  var _self = this;
  _self._privateProperty;
  return {
    getProperty: function() {
      return _self._privateProperty;
    }
  };
}
~~~

* Name your functions, for stack trace

~~~js
var log = function log(msg) {
  console.log(msg);  
};
console.log(log.name); // => 'log'

// Bad way
var log = function(msg) {
  console.log(msg);
};
console.log(log.name); // => ''
~~~

###### <a name="JS-110"></a> [JS-110] Accesors

* If you make use of the accesor functions use `getVal`, `setVal(12);` 

~~~js
// setting value 
person.setName('Alejandro');
// getting value 
person.getAge(); // => 12
~~~

* If the property is a boolean use isVal(), hasVal()

~~~js
person.hasName(); // => false
person.isReady(); // => true
~~~

###### <a name="JS-111"></a> [JS-111] Modules 

* Modules should be in one file, wrapped in an IIFE and `'use strict'`

~~~js
;(function(global) {
  'use strict';
  function UserModule() {
    var _name = 'unknown';
    return {
      getName: function getName() {
        return name;
      }
    };
  };
}(this));
~~~
###### <a name="JS-112"></a> [JS-112] Eval is Evil 

* Never use code evaluation

~~~js
// avoid 
setTimeout('myFunc(2)', 100);
eval('obj.' + property);
~~~

###### <a name="JS-113"></a> [JS-113] Type testing 

~~~js
// String
typeof variable === 'string';
// Number
typeof variable === 'number';
// Boolean
typeof variable === 'boolean';
// Object
typeof variable === 'object';
// Array
Array.isArray(object); 
// Null
typeof variable === null;
~~~