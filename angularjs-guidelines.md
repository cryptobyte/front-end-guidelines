# Veo AngularJS Front-End Guidelines

## Angular JS

#### Code Style 
* [NG-001 General Rules](#ng-001)
* [NG-002 Antipatterns](#ng-002)
* [NG-100 Naming Convention](#ng-100)
* [NG-101 Controller Definition](#ng-101)
* [NG-102 Directive Definition](#ng-102)
* [NG-103 Factory Definition](#ng-103)
* [NG-104 Service Definition](#ng-104)
* [NG-105 Module Definition](#ng-105)
* [NG-106 Communication](#ng-106)

### <a name="ng-001"></a> General Rules
* **Publish/Subscribe** : Only use `$broadcast`, `.$emit`, `.$on` for events that are relevant globally across the entire app
 - Authentification
 - App Closing 
 - Connetion Lost
 
* **Expressions** : Use expressions whenever possible, `ng-src`, `ng-href` supports `{{}}`.
* **Unbind on destroy** : Tear down your plugins and listeners. Every Controller and Directive emit an event right before they are destroyed. Subscribe to `$scope.$on('$destroy', ...)` 
* **Logic in Services** : Keep the logic in Services, logic may be reused by multiple controllers when placed withing a service. Logic in a service can more easily be isolated in a unit test and this can keep the controller slim. 

* **Controllers for Views** : Define controllers for a view , try to avoid resusing the controller for other views and move the logic to the factories.

* **Don't bind server data directly** : For binding data in templates normalize data to a simpler namespace. For directives use attributes to bind data this keeps directives isolated. This makes the view agnostic to data structure. 
* **Keyword `this`** : Keyword `this` is contextual and when used within a function inside a controller may change its context. Capture the context of `this` in a variable to avoid the problem. 

~~~js 
var _self = this;
~~~
* **Revealing Module Pattern for Directives and Factories** Use this pattern for Directives, Factories but keeping the method biding on top of the function. 

* **DOM in Directives** use directives for DOM manipulation, for example showing or hiding use `ngShow`, `ngHide` instead.

### <a name="ng-002"></a> Antipatterns
- Don't Wrap `element` inside of `$()` all elements are already jq-objects
- Don't use jQuery to create templates or DOM


### <a name="ng-100"></a> Naming Convention
~~~js
// Controllers
// Filename: play.controller.js
angular
  .module('veo')
  .controller('PlayController', PlayController);

// Directives
angular
  .module('veo')
  .directive('assetTitle', assetTitle);

// Filename: asset-title.directive.js
// Element: <asset-title></asset-title>
// Attribute: <a asset-title="playsection"></a>

// Factory
angular
  .module('veo')
  .factory('userFactory', userFactory);
~~~

### <a name="ng-101"></a> Controller Definition 
~~~js
// filename: header.controller.js
;(function() {
  'use strict';
  
  /**
  * @namespace HeaderController
  * @desc 
  */
  Veo.controller('HeaderController', ['$MenuServices', HeaderController]);
  
  function HeaderController($MenuServices) {
    var _self = this;
    var blocked = true;
    
    _self.name = 'HeaderController';
    _self.callSearch = callSearch;
    
    /**
    * @name callSearch
    * @param {Array} array of search results
    */
    function callSearch(results) {
      _self.results = results;
    };
  }
}());
~~~

### <a name="ng-102"></a> Directive Definition 

~~~js 
// filename: asset-title.directive.js
;(function() {
  'use strict';
  Veo.directive('assetTitle', assetTitle);
  
  /**
   * @namespace assetTitle
   * @desc 
   */
  function assetTitle() {
    var directive = {
      restrict: 'A',
      scope: {},
      templateUrl: '/template/path/template.html',
      link: link
    };
    
    function link(scope, element, attrs){
      // Do-stuff
    }
    
    return directive;
  }
}());
~~~

### <a name="ng-103"></a> Factory Defintion 
~~~js
// filename: user.factory.js
;(function() {
  'use strict';
  Veo.factory('userFactory', ['$http', userFactory]);

  /**
   * @namespace userFactory
   * @desc User data
   */
  function userFactory($http) {
    var url = '/user/details.json';
    var factory = {
      getDetails: getDetails
    };
    
    /**
     * @name getDetails
     * @desc gets the user details
     * @param {Function} callback
     */
    function getDetails(fn) {
      $http
        .get(url).success(function (data){
          fn(data);
        })
        .catch(function (error){
          fn({'success': false, 'error': 1001, 'message': 'user not found'});
        });
    }
    
    return factory;
  }
}());
~~~

### <a name="ng-104"></a> Service Definition 
~~~js
// filename: menu.service.js
;(function() {
  'use strict';
  Veo.factory('menuService', menuService);
  
  function menuService() {
    var _self = this;
    
    // method binding on top
    _self.menuItems = menuItems;
    _self.itemDetail = itemDetail;
    
    /**
     * @method menuItems
     * @desc returns an array of menu items
     * @param {String} id
     * @returns {Array}
     */
    function menuItems(id) {
      return menu;  
    };
    
    /**
     * @method itemDetail
     * @desc returns the Item details
     * @returns {Object}
     */
    function itemDetail() {
      return item;
    };
  }
}());
~~~

### <a name="ng-105"></a> Module Definition 
* Declare modules without a variable using the setter syntax

~~~js
angular
  .module('veo', [
    'ngRoute',
    'app.user',
    'app.shared'
  ]);

//Avoid module variables in the global they can be overriden 
var veo = angular
  .module('veo', [
    'ngRoute',
    'ngCookies'
  ]);
~~~

### <a name="ng-106"></a> Communication 
* **Controller** > **Directive** 
The communication between controllers naturally happens using the $scope binding. 

* **Controller** > **Controller**
The communication between controllers must to be through dependecy injection of singleton services with encapsulated state.

* **Directive** > **Directive** There shouldn't be a communication between isolated directives , unless they are compound components. 

