# Veo CSS/LESS Front-End Guidelines 

As with every guideline, this should be a simple guide to create readable and better code.

==**Always think ahead**==

## Content
* [[CSS-000] - Alphanumeric Order](#CSS-000)
* [[CSS-001] - Architectural Principles](#CSS-01)
* [[CSS-002] - Recomended Structure](#CSS-02)
* [[CSS-003] - File Code Structure](#CSS-03)
	* [[CSS-003.01] - Spacing](#CSS-03.01)
	* [[CSS-003.02] - 80 Characters Wide](#CSS-03.02)
	* [[CSS-003.03] - Titling](#CSS-03.03) 
* [[CSS-004] - Preprocessor Comments](#CSS-04)
* [[CSS-005] - Naming Conventions](#CSS-05)
	* [[CSS-005.01] - Hypen Delimited](#CSS-05.01)
* [[CSS-006] - Multi-line CSS](#CSS-06)
* [[CSS-007] - Alignment](#CSS-07)
* [[CSS-008] - Meaningful Whitespace](#CSS-08)
* [[CSS-009] - Indenting](#CSS-09)
   * [[CSS-009.01] - Indenting LESS](#CSS-09.01)
* [[CSS-010] - CSS Selectors](#CSS-10)
	* [[CSS-010.01] - General Rules](#CSS-10.01)
	* [[CSS-010.02] - Selector Intent](#CSS-10.02)
	* [[CSS-010.03] - Reusability](#CSS-10.03)
	* [[CSS-010.04] - Location Independence](#CSS-10.04)
	* [[CSS-010.05] - Portability](#CSS-10.05)
	* [[CSS-010.06] - Selector Performance](#CSS-10.06)
* [[CSS-NOT-001] - Antipatterns](#CSS-11)
   * [[CSS-NOT-001.01] - Unclear Naming](#CSS-11.01) 
   * [[CSS-NOT-001.02] - Bible blocks](#CSS-11.02)
   * [[CSS-NOT-001.03] - Unnecessary verboseness](#CSS-11.03)
   * [[CSS-NOT-001.04] - Browser Specific Overrides](#CSS-11.04)
   * [[CSS-NOT-001.05] - Overly Specific Classes](#CSS-11.05)
   * [[CSS-NOT-001.06] - Libraries](#CSS-11.06)
   
---

#### <a name="CSS-000">Alphanumeric Order</a>

Once upon a time, the alphabeth was invented to bring order and structure to our communication system. These rules have endured the pass of the year, so let's keep them rolling!

Facts are that we build a lot of code, and sometimes we need to scan for a piece of code that need's to be changed, fixed or improved.

~~~css
.contact {
  display: block;
  position: relative;
  top: 20px;
  right: 5px;
  height: 20px;
  margin: 22px 0 19px;
  padding-right: 25px;
  border: 0 none;
  font-family: 'OpenSans Semibold';
  text-transform: uppercase;
  font-size: 15px;
  color: #FFF;
}
~~~

When we go for a quick scann is faster to look at the first letter of the word we are seeing than reading the entire attributes and it's values.

~~~css
.contact {
  border: 0 none;
  color: #FFF;
  display: block;
  font-family: 'OpenSans Semibold';
  font-size: 15px;
  height: 20px;
  margin: 22px 0 19px;
  padding-right: 25px;
  position: relative;
  right: 5px;
  text-transform: uppercase;
  top: 20px;
}
~~~

Althoug *the lenght of the block is not a thing we should do*, it works perfectly as an example of the alphanumeric order.


#### <a name="CSS-01">Architecture Principle</a>

At a very high-level, your architecture should help you

* provide a consistent and sane environment;
* accommodate change;
* grow and scale your codebase;
* promote reuse and efficiency;
* increase productivity.

#### <a name="CSS-02">Recomended Scructure</a> 

* The structure should allow us to keep everything in the right place so we can add code without messing with someone else's code.

###### *SUGGESTED*

~~~css
/**
 * CONTENTS
 *
 * SETTINGS
 * Global...............Globally-available variables and config.
 *
 * TOOLS
 * Mixins...............Useful mixins.
 *
 * GENERIC
 * Normalize.css........A level playing field.
 * Box-sizing...........Better default `box-sizing`.
 *
 * BASE
 * Headings.............H1–H6 styles.
 *
 * OBJECTS
 * Wrappers.............Wrapping and constraining elements.
 *
 * COMPONENTS
 * Page-head............The main page header.
 * Page-foot............The main page footer.
 * Buttons..............Button elements.
 *
 * TRUMPS
 * Text.................Text helpers.
 */
~~~

#### <a name="CSS-03">File Code Structure</a>
Structures are meant to be used as the foundation of every major building, hence the need for us to have a structure, looking for a solid base from scratch.

##### <a name="CSS-03.01">Spacing</a>

We want to have **2 spaces** (*no tabs*) indenting, the reason for this is to be able to scan the code as fast as possible when needed, instead of reading all the lines trying to find that part of code we need to modify; also we want to unify the coding standards across the front-end.

*Remember that we will minify the code later, for developing we want to have the code as readable as possible*

~~~css
selector {  
  property: value;
    
  sub-selector { 
    property: value;
  }
}
~~~

Note that we have one (1) carrier return when defining a new sub selector, we'll talk about this later [here](#CSS-08), [here](#CSS-09) and [here](#CSS-09.01).

##### <a name="CSS-03.02">80 Characters Wide</a>

Where possible, limit CSS files’ width to 80 characters. Reasons for this include

* the ability to have multiple files open side by side;
* viewing CSS on sites like GitHub, or in terminal windows;
* providing a comfortable line length for comments.

~~~css
/**
 * I am a long-form comment. I describe, in detail, the CSS that follows. I am
 * such a long comment that I easily break the 80 character limit, so I am
 * broken across several lines.
 */
~~~

*There will be unavoidable exceptions to this rule—such as URLs, or gradient syntax—which shouldn’t be worried about.*


##### <a name="CSS-03.03">Titling</a>

Begin every new major section of a CSS project with a title:

~~~css
/*------------------------------------*\
    #SECTION-TITLE
\*------------------------------------*/

.selector {}
~~~

The title of the section is prefixed with a hash (#) symbol to allow us to perform more targeted searches (e.g. grep, etc.): instead of searching for just SECTION-TITLE—which may yield many results—a more scoped search of #SECTION-TITLE should return only the section in question.

Leave a carriage return between this title and the next line of code (be that a comment, some Sass, or some CSS).

If you are working on a project where each section is its own file, this title should appear at the top of each one. If you are working on a project with multiple sections per file, each title should be preceded by five (5) carriage returns. This extra whitespace coupled with a title makes new sections much easier to spot when scrolling through large files:

~~~css
/*------------------------------------*\
    #A-SECTION
\*------------------------------------*/

.selector {}





/*------------------------------------*\
    #ANOTHER-SECTION
\*------------------------------------*/

/**
 * Comment
 */

.another-selector {}
~~~
#### <a name="CSS-04">Preprocessor Comments</a>

With most—if not all—preprocessors, we have the option to write comments that will not get compiled out into our resulting CSS file. As a rule, use these comments to document code that would not get written out to that CSS file either. If you are documenting code which will get compiled, use comments that will compile also. For example, this is correct:

~~~css
// Dimensions of the @2x image sprite:
$sprite-width:  920px;
$sprite-height: 212px;

/**
 * 1. Default icon size is 16px.
 * 2. Squash down the retina sprite to display at the correct size.
 */
.sprite {
  background-image: url(/img/sprites/main.png);
  background-size: ($sprite-width / 2 ) ($sprite-height / 2); /* [2] */
  height: 16px; /* [1] */
  width:  16px; /* [1] */
}
~~~

In the previous example we have two types of comments:

* // the ones that are relative to the preprocessor we are using
* /* The ones that are CSS standard */

The preprocessor related will be removed when we compile our code, but the CSS based will remain. We should put our comments according to the code needs.

#### <a name="CSS-05">Naming Conventions</a>

Naming in CSS is very useful when sharing your code with others, that's the reason why we **must** implement a naming convention.

A good naming convention will allow the team members to know the follwing:

* what type of thing a class does;
* where a class can be used;
* what (else) a class might be related to;

We can use *Hyphen Delimited* naming, being this the most simple and useful when nesting selectors and it's properties, although it's not exclusive.

##### <a name="CSS-05.01">Hypen Delimited</a>

All strings in classes are delimited with a hyphen (-), like so:

~~~css
.page-head {}
.sub-content{}
~~~

Camel case and underscores are not user for regular clases, we should *avoid* camel case since we are not using them as vars in javascript.


#### <a name="CSS-06">Multi-Line CSS</a>

CSS should be written across multiple lines, except in very specific circumstances. There are a number of benefits to this:

* A reduced chance of merge conflicts, because each piece of functionality exists on its own line.
* More ‘truthful’ and reliable *diffs*, because one line only ever carries one change.

Exceptions to this rule should be fairly apparent, such as similar rulesets that only carry one declaration each, for example:

~~~css
.icon {
  background-image: url(/img/sprite.svg);
  display: inline-block;
  height: 16px;
  width:  16px;
}

.icon--home     { background-position:   0     0  ; }
.icon--person   { background-position: -16px   0  ; }
.icon--files    { background-position:   0   -16px; }
.icon--settings { background-position: -16px -16px; }
~~~

These types of ruleset benefit from being single-lined because

* they still conform to the one-reason-to-change-per-line rule;
* they share enough similarities that they don’t need to be read as thoroughly as other rulesets—there is more benefit in being able to scan their selectors, which are of more interest to us in these cases.

#### <a name="CSS-07">Alignment</a>
Attempt to align common and related identical strings in declarations, for example:

~~~css
.foo {
  -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
      border-radius: 3px;
}

.bar {
  bottom: 0;
  left:   0;
  margin-right: -10px;
  margin-left:  -10px;
  padding-right: 10px;
  padding-left:  10px;
  position: absolute;
  right:  0;
  top:    0;
}
~~~

#### <a name="CSS-08">Meaningful Whitespace</a>

Whitespace can be as useful as indentation when nesting or grouping related code.

The *suggestion* is to use:

* One (1) empty line between closely related rulesets.
* Two (2) empty lines between loosely related rulesets.
* Five (5) empty lines between entirely new sections.

~~~css
/*------------------------------------*\
    #FOO
\*------------------------------------*/

.foo {}

  .foo__bar {}


.foo--baz {}





/*------------------------------------*\
    #BAR
\*------------------------------------*/

.bar {}

  .bar__baz {}

  .bar__foo {}
~~~

#### <a name="CSS-09">Indenting</a>
As well as indenting individual declarations, indent entire related rulesets to signal their relation to one another, for example:

~~~css
.foo {}

  .foo__bar {}

    .foo__baz {}
~~~
By doing this, a developer can see at a glance that *.foo__baz {}* lives inside *.foo__bar {}* lives inside *.foo {}*.

This quasi-replication of the DOM tells developers a lot about where classes are expected to be used without them having to refer to a snippet of HTML.

##### <a name="CSS-09.01">Indenting LESS</a>

LESS provides nesting functionality. That is to say, by writing this:

~~~css
.foo {
  color: red;

  .bar {
    color: blue;
  }

}
~~~

As told before, we are writing code that we can read, so later when we minify the code we will end with this:

~~~css
.foo { color: red; }
.foo .bar { color: blue; }
~~~

LESS nesting should be avoided wherever possible, the short reason is that we don't over write the code that we may later write, for more information on this you can check [this article on Specificity](2).

#### <a name="CSS-10">CSS Selectors</a>

One of the most ==important and critical== aspect of writing ==maintainable and scalable CSS== is selectors.

##### <a name="CSS-10.01">General Rules</a>

Your selectors are fundamental to writing good CSS. To very briefly sum up the above sections:

* **Select what you want explicitly**, rather than relying on circumstance or coincidence. Good Selector Intent will rein in the reach and leak of your styles.
* **Write selectors for reusability**, so that you can work more efficiently and reduce waste and repetition.
* **Do not nest selectors unnecessarily**, because this will increase specificity and affect where else you can use your styles.
* **Do not qualify selectors unnecessarily**, as this will impact the number of different elements you can apply styles to.
* **Keep selectors as short as possible**, in order to keep specificity down and performance up.

Focussing on these points will keep your selectors a lot more sane and easy to work with on changing and long-running projects.

##### <a name="CSS-10.02">Selector Intent</a>
When writing our CSS we must scope our selectors correctly, and be sure that we are selecting the right things for the right reasons. For instance:

~~~css
header ul {}
~~~

The code above will affect to all the ul elements that are inside a header element. If we are aiming just to modify the attributes to our navigation menu a better approach would be:

~~~css
.site-nav {}
~~~

==Selector Intent is the process of thinking ahead on our code and name as best and explicit as we can before moving forward on our code.==

CSS cannot be encapsulated, it is inherently leaky, but we can mitigate some of these effects by not writing such globally-operating selectors: **your selectors should be as explicit and well reasoned as your reason for wanting to select something**.

##### <a name="CSS-10.03">Reusability</a>

With a move toward a more component-based approach to constructing UIs, the idea of reusability is paramount. We want the option to be able to move, recycle, duplicate, and syndicate components across our projects.

==Everything you choose, from the type of selector to its name, should lend itself toward being reused.==

##### <a name="CSS-10.04">Location Independence</a>

Given the ever-changing nature of most UI projects, and the move to more component-based architectures, it is in our interests not to style things based on where they are, but on what they are.

Let's have an example, where we want to style an `<a>` tag inside another given selector:

~~~css
.selector a {}
~~~

Not only it's a bad *Selector Intent* but also will affect all the `<a>` tags inside the `.selector` element.

~~~css
.btn {}
~~~

This approach would be a best option that we can reuse not only side our `.selector` but anywhere we put that *class name*. This is a location independent element.

##### <a name="CSS-10.05">Portability</a>

With Location Independent elements we gained the option to move around the styling for the elements no matter where we used them, but this have a bigger approach than that, and this is portability.

But this is only a mere concept that will change the way we use independent elements.

~~~css
input.btn {}
~~~

In the previous code we are apparently removing the independence of the `.btn` class name by assigning it to an `input` tag, but what we are really doing is to define the portability of the `.btn` class name by relating it to another `tag name`.

Let's take the next example to clarify a bit more about this:

~~~css
/**
 * Embolden and colour any element with a class of `.error`.
 */
.error {
  color: red;
  font-weight: bold;
}

/**
 * If the element is a `div`, also give it some box-like styling.
 */
div.error {
  border: 1px solid;
  padding: 10px;
}
~~~

This is a nice appraoch to portability and independent elements, but we should see the bigger picture before assigning class names and elements with it's own style.

We should consider, in the example, that we will handle errors across our site, so an even better approach to this would like:

~~~css
/**
 * Text-level errors.
 */
.error-text {
  color: red;
  font-weight: bold;
}

/**
 * Elements that contain errors.
 */
.error-box {
  border: 1px solid;
  padding: 10px;
}
~~~

This means that we can apply `.error-box` to any element instead of only `div` elements.

A very important thing to consider when naming things is the specificity of that element.

~~~css
/**
 * Runs the risk of becoming out of date; not very maintainable.
 */
.blue {
  color: blue;
}

/**
 * Depends on location in order to be rendered properly.
 */
.header span {
  color: blue;
}

/**
 * Too specific; limits our ability to reuse.
 */
.header-color {
  color: blue;
}

/**
 * Nicely abstracted, very portable, doesn’t risk becoming out of date.
 */
.highlight-color {
  color: blue;
}
~~~

==We **must** always observe that the code we are building and naming could have more than one use, so the best approach to naming the elements is a middle point: between being too specific that we can't reuse our element, and being too obvious that it will end as a redundant name.==

##### <a name="CSS-10.06">Selector Performance</a>

Generally speaking, the longer a selector is (i.e. the more component parts) the slower it is, for example:

~~~css
body.home div.header ul {}
~~~

…is a far less efficient selector than:

~~~css
body.home div.header ul {}
~~~

This is because browsers read CSS selectors right-to-left.

==This is just one reason why **nesting with preprocessors is often a false economy**; as well as making selectors unnecessarily more specific, and creating location dependency, it also creates more work for the browser.==

#### <a name="CSS-11">Antipatterns</a>

We have covered a lot of stuff that we **should** do with the previous code standards, but there a few things left that we need to clarify so we can avoid them. Basically the things we should ==**NOT**== do.

##### <a name="CSS-11.01">Unclear Naming</a>

We may have learned about the naming standard, but even knowing that we can decide for a quick bad name when we just want to try something, which is not that bad, but at the end we will end moving names around and building the code 1.5x times instead of thinking ahead and naming accordingly to what we are building.

~~~css
.qcktst {
  color: lightgray;
  font-size: small;
}
~~~

Both *class names* and *attributes* are subject to this unclear naming virus, so we must avoid them in order to stablish a *think-ahead* workflow.

##### <a name="CSS-11.02">Bible blocks</a>

Building big, large and with a ton of information blocks won't comply with the specificity and structure conventions. This will simply break our entire code conventions because they won't be reusable, portable and component-specific.

~~~css
.contact {
  border: 0 none;
  color: #FFF;
  display: block;
  font-family: 'OpenSans Semibold';
  font-size: 15px;
  height: 20px;
  margin: 22px 0 19px;
  padding-right: 25px;
  position: relative;
  right: 5px;
  text-transform: uppercase;
  top: 20px;
}
~~~

##### <a name="CSS-11.03">Unnecessary verboseness</a>

Soemtimes we will need to specify every value of a common attribute because the design will ask for it in order to keep the images out of the webpage assets as much as possible:

~~~css
.selectorX {
  background: #2175BE;
  background-image: url(../images/bottomshadownav.png);
  background-position: bottom;
  background-repeat: repeat-x;
}
~~~

Some other times (most of the times due design trends) we should simply include all the values for that attribute in the same line:

~~~css
.selectorX {
  background: #2175BE url(../images/bottomshadownav.png) repeat-x bottom;
}
~~~

##### <a name="CSS-11.04">Browser Specific Overrides</a>

Let's just say it as it is: Internet Explorer is the one to blame for this. But we should avoid as much as possible the browser specific overrides, we are coding hacks for browsers that are the minority of the web and sometimes the fix is not on the css but with some other solutions that look for the big picture, like the old *html5shiv*.

~~~css
.decile img {
  _margin: 0px /* IE 6 Hack */
}
~~~

##### <a name="CSS-11.05">Overly Specific Classes</a>

When we talked about structure and how it makes everyone's life easier we also, implicitly, talked about planning ahead, studying our designs and all the necessary stuff we were going to need for our code.

That's exactly what will prevent us to double work the same without noticing it.

For instance, we have this small block:

~~~css
.slider {
  height: 27px;
  float: left;
}
~~~
  
And a few weeks after, without any proper structure nor comments nor documentation on the documents we already have built, we could end with something like this:
  
~~~css
.slider {
  height: 27px;
}

.left {
  float: left;
}
~~~

##### <a name="CSS-11.06">Libraries</a>

It's okay to recognize that we have a lot of libraries in the web to make the front-end css part way easier than it was 5 years ago, but we should avoid the extensive use of that libraries to keep our code clean, simple and fast.

---

For further information you can go to [cssguidelin.es](3) and read the entire document with a lot more info, including js and HTML conventions. We are only taking the useful part of CSS that we will implement.

[1]: http://cssguidelin.es/#bem-like-naming
[2]: http://cssguidelin.es/#specificity
[3]: http://cssguidelin.es/